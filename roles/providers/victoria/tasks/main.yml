---
- name: Provision victoria-metrics
  block:
    - name: Creating app directory
      file:
        path: "{{ app_dir }}"
        state: directory
        mode: '0755'
        owner: "apollo"
        group: "apollo"

    - name: Creating storage directory
      file:
        path: "{{ arc['data']['volumes_dir'] }}/{{ app_name }}"
        state: directory
        mode: '0755'
        owner: "apollo"
        group: "apollo"

    - name: Create config directories
      file:
        path: "{{ app_dir }}/{{ config_dir.path }}"
        state: directory
        mode: '{{ config_dir.mode }}'
      with_filetree:
        - "files/"
      when: config_dir.state == 'directory'
      loop_control:
        label: "{{ config_dir.path }}"
        loop_var: config_dir

    - name: Copy config files
      copy:
        src: "{{ config_file.src }}"
        dest: "{{ app_dir }}/{{ config_file.path }}"
        mode: "{{ config_file.mode }}"
      with_filetree:
        - "files/"
      when: config_file.state == 'file'
      loop_control:
        label: "{{ config_file.path }}"
        loop_var: config_file

    # - name: Setup federation
    #   block:
    #     - name: Access federated Spaces
    #       git:
    #         repo: "{{ space }}"
    #         dest: "{{ apollo_downloads_dir }}/prometheus-federation-space-{{ space_index }}"
    #       with_items:
    #         - "{{ prometheus_federation_spaces }}"
    #       register: prometheus_federation_directories
    #       loop_control:
    #         loop_var: space
    #         index_var: space_index

    #     - name: Debug federated Spaces
    #       debug:
    #         msg: "{{ prometheus_federation_directories }}"
    #   when: prometheus_federation_enabled|bool

    # # https://github.com/prometheus/prometheus/issues/5976
    # - name: Correct volume permissions
    #   file:
    #     path: /var/lib/docker/volumes/prometheus_prometheus/
    #     state: directory
    #     recurse: yes
    #     owner: "65534"
    #     group: "65534"
    #   when: arc['data']['provider'] == "generic"

    # TODO: find volume node + id via  cio volume inspect prometheus_prometheus-data
    # - name: Correct volume permissions
    #   file:
    #     path: /cio/volumes/prometheus_prometheus/
    #     state: directory
    #     recurse: yes
    #     owner: "65534"
    #     group: "65534"
    #   when: arc['data']['provider'] == "storidge"

    - name: Check for legacy Docker volume
      stat:
        path: "/var/lib/docker/volumes/prometheus_victoria-data/_data"
      register: volume_stat_result

    - name: Check if volume is already migrated
      stat:
        path: "{{ arc['data']['volumes_dir'] }}/{{ app_name }}/flock.lock"
      register: stat_result

    - name: Migrate volume
      copy:
        src: /var/lib/docker/volumes/prometheus_victoria-data/_data
        dest: "{{ arc['data']['volumes_dir'] }}/{{ app_name }}"
        remote_src: yes
      when: 
        - stat_result is defined
        - not stat_result.stat.exists
        - volume_stat_result.stat.exists

    - block:
      - name: Download victoria-metrics binary
        become: false
        get_url:
          url: "https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v{{ victoria_version }}/victoria-metrics-v{{ victoria_version }}.tar.gz"
          dest: "{{ apollo_downloads_dir }}/victoria-metrics-v{{ victoria_version }}.tar.gz"
        register: _download_binary
        until: _download_binary is succeeded
        retries: 5
        delay: 2
        check_mode: false
  
      - name: Unpack victoria-metrics binary
        become: false
        unarchive:
          remote_src: True
          src: "{{ apollo_downloads_dir }}/victoria-metrics-v{{ victoria_version }}.tar.gz"
          dest: "{{ apollo_downloads_dir }}"
          creates: "{{ apollo_downloads_dir }}/victoria-metrics-prod"
        check_mode: false
  
      - name: Propagate vmagent binaries
        copy:
          src: "{{ apollo_downloads_dir }}/victoria-metrics-prod"
          dest: "{{ apollo_binary_dir }}/victoria-metrics"
          mode: 0755
          owner: apollo
          group: apollo
          remote_src: True
        notify: restart victoria
        when: not ansible_check_mode

      - name: Copy the victoria-metrics systemd service file
        template:
          src: templates/victoria.service.j2
          dest: /etc/systemd/system/victoria.service
          owner: root
          group: root
          mode: 0644
        notify: restart victoria

      - name: Make sure service is started
        service:
          name: victoria
          state: started
          enabled: true

    - block:
      - name: Download blackbox-exporter binary
        become: false
        get_url:
          url: "https://github.com/prometheus/blackbox_exporter/releases/download/v{{ blackbox_version }}/blackbox_exporter-{{ blackbox_version }}.linux-amd64.tar.gz"
          dest: "{{ apollo_downloads_dir }}/blackbox_exporter-v{{ blackbox_version }}.tar.gz"
        register: _download_binary
        until: _download_binary is succeeded
        retries: 5
        delay: 2
        check_mode: false
  
      - name: Unpack blackbox-exporter binary
        become: false
        unarchive:
          remote_src: True
          src: "{{ apollo_downloads_dir }}/blackbox_exporter-v{{ blackbox_version }}.tar.gz"
          dest: "{{ apollo_downloads_dir }}"
          creates: "{{ apollo_downloads_dir }}/blackbox_exporter-{{ blackbox_version }}.linux-amd64/blackbox_exporter"
        check_mode: false
  
      - name: Propagate blackbox-exporter binaries
        copy:
          src: "{{ apollo_downloads_dir }}/blackbox_exporter-{{ blackbox_version }}.linux-amd64/blackbox_exporter"
          dest: "{{ apollo_binary_dir }}/blackbox_exporter"
          mode: 0755
          owner: apollo
          group: apollo
          remote_src: True
        notify: restart blackbox_exporter
        when: not ansible_check_mode

      - name: Remove legacy blackbox-exporter config
        file:
          path: "{{ app_dir }}/blackbox.yml"
          state: absent

      - name: Propagate blackbox-exporter config
        template:
          src: templates/blackbox_exporter.yml.j2
          dest: "{{ app_dir }}/blackbox_exporter.yml"
          owner: apollo
          group: apollo
          mode: '0644'
        notify: restart blackbox_exporter

      - name: Copy the blackbox-exporter systemd service file
        template:
          src: templates/blackbox_exporter.service.j2
          dest: /etc/systemd/system/blackbox_exporter.service
          owner: root
          group: root
          mode: 0644
        notify: restart blackbox_exporter
  
      - name: Make sure service is started
        service:
          name: blackbox_exporter
          state: started
          enabled: true

    - block:
      - name: Download vmagent binary
        become: false
        get_url:
          url: "https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v{{ vmagent_version }}/vmutils-v{{ vmagent_version }}.tar.gz"
          dest: "{{ apollo_downloads_dir }}/vmutils-v{{ vmagent_version }}.tar.gz"
        register: _download_binary
        until: _download_binary is succeeded
        retries: 5
        delay: 2
        check_mode: false
  
      - name: Unpack vmagent binary
        become: false
        unarchive:
          remote_src: True
          src: "{{ apollo_downloads_dir }}/vmutils-v{{ vmagent_version }}.tar.gz"
          dest: "{{ apollo_downloads_dir }}"
          creates: "{{ apollo_downloads_dir }}/vmagent-prod"
        check_mode: false
  
      - name: Propagate vmagent binaries
        copy:
          src: "{{ apollo_downloads_dir }}/vmagent-prod"
          dest: "{{ apollo_binary_dir }}/vmagent"
          mode: 0755
          owner: apollo
          group: apollo
          remote_src: True
        notify: restart vmagent
        when: not ansible_check_mode

      - name: Propagate vmagent config
        template:
          src: templates/prometheus.yml.j2
          dest: "{{ app_dir }}/prometheus.yml"
          owner: apollo
          group: apollo
          mode: '0644'
        notify: restart vmagent

      - name: Copy the vmagent systemd service file
        template:
          src: templates/vmagent.service.j2
          dest: /etc/systemd/system/vmagent.service
          owner: root
          group: root
          mode: 0644
        notify: restart vmagent

      - name: Make sure service is started
        service:
          name: vmagent
          state: started
          enabled: true
