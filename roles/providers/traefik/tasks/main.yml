---
# tasks file for zero-app-traefik
- name: Provision Traefik
  block:
    - name: Creating app directory
      file:
        path: "{{ app_dir }}"
        state: directory
        mode: '0755'
        owner: "apollo"
        group: "apollo"

    - name: Create config directories
      file:
        path: "{{ app_dir }}/{{ config_dir.path }}"
        state: directory
        mode: '{{ config_dir.mode }}'
        owner: "apollo"
        group: "apollo"
      with_filetree:
        - "files/"
      when: config_dir.state == 'directory'
      loop_control:
        label: "{{ config_dir.path }}"
        loop_var: config_dir

    - name: Copy config files
      copy:
        src: "{{ config_file.src }}"
        dest: "{{ app_dir }}/{{ config_file.path }}"
        mode: "{{ config_file.mode }}"
        owner: "apollo"
        group: "apollo"
      with_filetree:
        - "files/"
      when: config_file.state == 'file'
      loop_control:
        label: "{{ config_file.path }}"
        loop_var: config_file

    - name: "Download {{ app_name }} binary"
      become: false
      get_url:
        url: "https://github.com/containous/traefik/releases/download/v{{ traefik_binary_version }}/traefik_v{{ traefik_binary_version }}_linux_amd64.tar.gz"
        dest: "{{ apollo_downloads_dir }}/traefik-v{{ traefik_binary_version }}.tar.gz"
      register: _download_binary
      until: _download_binary is succeeded
      retries: 5
      delay: 2
      check_mode: false

    - name: "Unpack {{ app_name }} binary"
      become: false
      unarchive:
        remote_src: True
        src: "{{ apollo_downloads_dir }}/traefik-v{{ traefik_binary_version }}.tar.gz"
        dest: "{{ apollo_downloads_dir }}"
        creates: "{{ apollo_downloads_dir }}/traefik"
      check_mode: false

    - name: "Propagate {{ app_name }} binary"
      copy:
        src: "{{ apollo_downloads_dir }}/traefik"
        dest: "{{ apollo_binary_dir }}/traefik"
        mode: 0755
        owner: root
        group: root
        remote_src: True
      notify: restart traefik
      when: not ansible_check_mode

    - name: "Copy {{ app_name }} systemd service file"
      template:
        src: templates/traefik.service.j2
        dest: /etc/systemd/system/traefik.service
        owner: root
        group: root
        mode: 0644
      notify: restart traefik

    - name: Copy apollo Traefik config
      template:
        src: templates/apollo-traefik.toml.j2
        dest: "{{ app_dir }}/apollo-traefik.toml"
        owner: "apollo"
        group: "apollo"
        mode: '0644'
      notify: restart traefik

    - name: Copy apollo Traefik dynamic config
      template:
        src: templates/apollo-traefik-dynamic.toml.j2
        dest: "{{ app_dir }}/apollo-traefik-dynamic.toml"
        owner: "apollo"
        group: "apollo"
        mode: '0644'
      notify: restart traefik

    - name: Make sure service is started
      service:
        name: traefik
        state: started
        enabled: true

    - name: Remove Docker Traefik config
      file:
        path: "{{ app_dir }}/traefik.toml"
        state: absent
